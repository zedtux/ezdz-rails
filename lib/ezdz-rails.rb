require 'ezdz-rails/version'

module Ezdz
  module Rails
    class Engine < ::Rails::Engine
      # Make assets avaiable
    end
  end
end
